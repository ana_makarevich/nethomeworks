﻿using System;

namespace GossipProtocol
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Gossip Protocol Simulation!");

            try
            {
                Console.Write("Enter the number of nodes in the cluster:");
                var n_nodes = int.Parse(Console.ReadLine());
                Console.Write("Enter the number of processes to track:");
                int n_processes = int.Parse(Console.ReadLine());
                Console.WriteLine("Please wait. We're setting up the nodes.");
                GossipSimulation sim = new GossipSimulation(n_nodes, n_processes);
                sim.RunSimulation();

            }
            catch (System.FormatException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Come back when you learn some integers!");
            }


        }
    }
}
