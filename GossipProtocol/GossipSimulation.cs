﻿using System;
namespace GossipProtocol
{
    public class GossipSimulation
    {
        /// <summary>
        /// This class creates a one-cluster simulation with the given 
        /// number of nodes. First it creates N nodes with identical processes and
        /// records the current datetime as the update time. 
        /// </summary>
        Cluster cluster;
        int n_nodes;
        int n_processes;
        int n_heartbeats;

        public GossipSimulation(int n_nodes_, int n_processes_)
        {
            // create cluster
            cluster = new Cluster();
            n_nodes = n_nodes_;
            n_processes = n_processes_;
            n_heartbeats = (int)((n_processes * n_nodes) / 2);
            // add n_nodes to the cluser
            for (int i = 0; i < n_nodes; i++) {
                cluster.AddNode(new Node(i));
            }
            // generate processes for all nodes
            SimulateEntries();
        }

        /// <summary>
        /// Creates 4 processes with ids from 1 to 4 and fixed heartbeats and
        /// the current date as the update date.
        /// </summary>
        public void SimulateEntries() {
            int heartbeat = 10100;
            for (int i = 1; i <= n_processes; i++) {
                cluster.AddProcessToAll(i, heartbeat);
                heartbeat += 4;
                System.Threading.Thread.Sleep(500);
            }
            
        }

        public void SimulateHeartbeats(int n_beats, int n_processes) {
            cluster.RunRandomHearbeats(n_beats, n_processes);
        }

        public void PrintSimulationState(string state_name) {
            Console.WriteLine(state_name);
            cluster.PrintAll();
            Console.WriteLine("\n");
            cluster.PrintNodeDetails();

        }
        public void RunSimulation() {
            // show the current state:
            PrintSimulationState("Inital Nodes");

            SimulateHeartbeats(n_heartbeats, n_processes);
            PrintSimulationState("Random Heartbeats");

            cluster.Gossip();
            PrintSimulationState("Gossip");

            cluster.KillDead();
            PrintSimulationState("Kill the dead!");






        }

    }
}
