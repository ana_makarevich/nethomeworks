﻿using System;
namespace GossipProtocol
{
    public class Cluster
    {
        public Node Head;
        public Node CurrentNode;

        /// <summary>
        /// Initializes a new instance of 
        /// the <see cref="T:GossipProtocol.Cluster"/> class. Empty cluster
        /// </summary>
        public Cluster() {
            Head = null;
            CurrentNode = null;
        }
        /// <summary>
        /// Initializes a new instance of 
        /// the <see cref="T:GossipProtocol.Cluster"/> class. Cluster with one
        /// node.
        /// </summary>
        /// <param name="node">Node.</param>
        public Cluster(Node node)
        {
            Head = node;
            CurrentNode = node;
        }

        /// <summary>
        /// Adds the node to the cluster
        /// </summary>
        /// <param name="node">Node instance</param>
        public void AddNode(Node node) {
            if (Head == null)
            {
                Head = node;
                CurrentNode = node;
            }
            else
            {
                CurrentNode.Next = node;
                CurrentNode = node;
            }
        }
        /// <summary>
        /// Prints the list of all nodes. 
        /// </summary>
        public void PrintAll() {
			Console.Write("Head ->");
			Node current_node = Head;
			while (current_node != null)
			{
				Console.Write(current_node.Id);
                Console.Write("->");
                current_node = current_node.Next;
				
			}
			Console.Write("NULL");

        }

        /// <summary>
        /// Prints the node details.
        /// </summary>
        public void PrintNodeDetails() {
            Node current_node = Head;
            while (current_node != null) {
                current_node.PrintHeartbeatTable();
                current_node = current_node.Next;
            }
        }

        /// <summary>
        /// Adds the process to all nodes.
        /// </summary>
        /// <param name="process_id">Process identifier.</param>
        /// <param name="heartbeat">Heartbeat value.</param>
        public void AddProcessToAll(int process_id, int heartbeat) {
            Node current_node = Head;
			while (current_node != null)
			{
                current_node.AddProcess(process_id, heartbeat);
				current_node = current_node.Next;
			}
            
        }

        /// <summary>
        /// Updates heartbeats for randomly chosen processed on nodes. 
        /// </summary>
        /// <param name="n_beats">Number of heartbeats to simulate</param>
        /// <param name="n_processes">Expected number of processes on node</param>
        public void RunRandomHearbeats(int n_beats, int n_processes) {
            // set counter to track the number of simulated beats
            int counter = n_beats;
            // starting node for iteration
            Node current_node = Head;
            // random numbers generator
            Random rnd = new Random();
            // process id to send heartbeat
            int process_id;
            // loop while we haven't simulated enough heartbeats
            while (counter > 0) 
            {
                // get the process id to update
                process_id = rnd.Next(1, n_processes);
                // update heartbeat for the chosen process
                current_node.SimulateHeartbeat(process_id);
                // move to the next node 
                current_node = current_node.Next;
                // if we've reached the end of the queue, 
                // start from the beginning
                if (current_node == null) {
                    current_node = Head;
                }
                counter--;
                    
            }
        }
        /// <summary>
        /// Randomly gossip nodes
        /// </summary>
        public void Gossip() {
            // we gossip the head in any case because this is our 
            // reference node for removing entries (for the sake of simulation) 
            Node current_node = Head;
            while (current_node != null)
            {
                Random rnd = new Random();
                bool gossip = rnd.Next(0, 1) == 0;
                if (gossip || current_node == Head) {
                    GossipNode(current_node);
                }
                current_node = current_node.Next;

            }
        }

        /// <summary>
        /// Gossips the given node to all the nodes in the cluster
        /// </summary>
        /// <param name="node">Node.</param>
        private void GossipNode(Node node) {
            Node current_node = Head;
            while (current_node != null) {
                if (current_node != node) {
                    node.Gossip(current_node);
                }
                current_node = current_node.Next;
            }
        }

        public void KillDead() {
            Node current_node = Head;
            while (current_node != null) {
                current_node.CleanNode();
                
                current_node = current_node.Next;
            }
        }

    }
}
