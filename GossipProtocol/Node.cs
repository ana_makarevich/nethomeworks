﻿using System;
namespace GossipProtocol
{
    public class Node
    {
        public int Id { get; set;} // ID of the node
        int Size { get; set;  } // size of the node - number of entries
        HearbeatsTable HTable { get; set;  }
        public Node Next;

        /// <summary>
        /// Initializes a new instance of 
        /// the <see cref="T:GossipProtocol.Node"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public Node(int id)
        {
            Id = id;
            Next = null;
            HTable = new HearbeatsTable();
        }

        /// <summary>
        /// Simulates the heartbeat for the proccess with the specified id. 
        /// </summary>
        /// <param name="process_id">Process identifier.</param>
        public void SimulateHeartbeat(int process_id) {
            HTable.UpdateProcess(process_id);
        }

        /// <summary>
        /// Adds the process to the node.
        /// </summary>
        /// <param name="process_id">Process identifier.</param>
        /// <param name="heartbeat">Heartbeat value.</param>
        public void AddProcess(int process_id, int heartbeat) {
            HTable.AddEntry(process_id, heartbeat);
        }

        /// <summary>
        /// Update the heartbeat table of the specified node 
        /// with the data from THIS node's heartbeat table. 
        /// </summary>
        /// <returns>The gossip.</returns>
        /// <param name="target">Target node - the node that will be updated.</param>
        public void Gossip(Node target) {
            target.HTable.SyncHeartbeats(HTable.HTable);
        }

        public void CleanNode() {
            HTable.RemoveDead();
        }

        /// <summary>
        /// Prints the heartbeat table.
        /// </summary>
        public void PrintHeartbeatTable() {
            Console.WriteLine(String.Format("Heartbeat Table for Node # {0}", Id));
            HTable.PrintTable();
        }
    }
}
