﻿using System;
using System.Data;

namespace GossipProtocol
{
	public class HearbeatsTable
	{
        public const double tFail = 1;
        public DataTable HTable {get; set;} 

        /// <summary>
        /// Initializes a new instance of 
        /// the <see cref="T:GossipProtocol.HearbeatsTable"/> class.
        /// </summary>
		public HearbeatsTable()
		{
            HTable = new DataTable();
			// create 3 columns to store data
			DataColumn column;
            column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "process_id"
            };
            HTable.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "heartbeat"
            };
            HTable.Columns.Add(column);
            column = new DataColumn
            {
                DataType = Type.GetType("System.DateTime"),
                ColumnName = "last_updated"
            };
            HTable.Columns.Add(column);
		}

        /// <summary>
        /// Adds the entry to the heartbeat table. 
        /// </summary>
        /// <param name="process_id">Process identifier.</param>
        /// <param name="heartbeat">Heartbeat.</param>
        public void AddEntry(int process_id, int heartbeat) {

            DataRow row = HTable.NewRow();
            row["process_id"] = process_id;
            row["heartbeat"] = heartbeat;
            row["last_updated"] = DateTime.Now;
            HTable.Rows.Add(row);
            
        }

        /// <summary>
        /// Updates the process with the specified id.
        /// </summary>
        /// <param name="process_id">Process identifier.</param>
        public void UpdateProcess(int process_id) {
            Random rnd = new Random();
            int heartbeat_increment = rnd.Next(1, 10);
            int time_increment = rnd.Next(1, 40);
            DataRow[] foundRows;
            String select_statement = String.Format("process_id = {0}", new object[] { process_id });
            foundRows = HTable.Select(select_statement);
            if (foundRows.Length > 0) {
                foundRows[0]["heartbeat"] = (int)foundRows[0]["heartbeat"] + heartbeat_increment;
                foundRows[0]["last_updated"] = DateTime.Now + new TimeSpan(0, 0, 0, time_increment);
            }

        }

        /// <summary>
        /// Syncs the heartbeats. Updates all the heartbeats in THIS table according to the passed table.
        /// </summary>
        /// <param name="table">Table.</param>
        public void SyncHeartbeats(DataTable table) {
            foreach (DataRow row in table.Rows) {
                // get id and heartbeatof the processed row
                int id = (int)row["process_id"];
                int heartbeat = (int)row["heartbeat"];
                // find 
                DataRow[] foundRows;
                // try to find the process in the heartbeat table with the same id but smaller heartbeat
                String select_statement = String.Format("process_id = {0} and heartbeat < {1}", new object[] {id, heartbeat} );
                foundRows = HTable.Select(select_statement);
                // if processes found in the current table (heartbeat_table)
                if (foundRows.Length > 0)
                {
                    // update heartbeat, set last_updated time to current time
                    foundRows[0]["heartbeat"] = heartbeat;
                    foundRows[0]["last_updated"] = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Prints the heartbeat table.
        /// </summary>
        public void PrintTable() {
            Console.WriteLine("ID    | Heartbeat         | Last Updated    ");
            foreach (DataRow dataRow in HTable.Rows)
               
			{
                String tablerow = String.Format("{0}     | {1}             |{2}    ", new object[] { dataRow[0], dataRow[1], dataRow[2] });	
                Console.WriteLine(tablerow);
			}
            
        }

        /// <summary>
        /// Checks if the process is dead. 
        /// </summary>
        /// <returns><c>true</c>, if the process is dead, <c>false</c> otherwise.</returns>
        /// <param name="row">Row.</param>
        public bool IsDead(DataRow row) {
            double update_period = (DateTime.Now - (DateTime)row["last_updated"]).TotalSeconds;
            return update_period > tFail;
        }

        /// <summary>
        /// Removes the dead processes from the table. 
        /// </summary>
        public void RemoveDead() {
            for (int i = HTable.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = HTable.Rows[i];
                if (IsDead(row))
                {
                    HTable.Rows.Remove(row);
                }
            }
            
        }

	}
}
