﻿using NUnit.Framework;
using System;
using GossipProtocol;
using System.Data;

namespace GossipProtocolTests
{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void TestTableCreation()
        {
            HearbeatsTable htable = new HearbeatsTable();
            htable.AddEntry(1, 25);
            String select_statement = String.Format("process_id = 1 and heartbeat < 35");
            DataRow[] foundRows = htable.HTable.Select(select_statement);
            Assert.AreEqual(1, foundRows.Length);
        }

        [Test()]
        public void TestTableUpdate() 
        {
            HearbeatsTable htable1 = new HearbeatsTable();
            htable1.AddEntry(1, 25);
            htable1.AddEntry(2, 35);
            htable1.AddEntry(3, 34);

            HearbeatsTable htable2 = new HearbeatsTable();
            htable2.AddEntry(1, 10);
            htable2.AddEntry(2, 40);
            htable2.AddEntry(3, 60);

            htable1.SyncHeartbeats(htable2.HTable);

			String select_statement1 = String.Format("process_id = 1");
            String select_statement2 = String.Format("process_id = 2");
            String select_statement3 = String.Format("process_id = 3");
            Assert.AreEqual(25, htable1.HTable.Select(select_statement1)[0]["heartbeat"]);
            Assert.AreEqual(40, htable1.HTable.Select(select_statement2)[0]["heartbeat"]);
            Assert.AreEqual(60, htable1.HTable.Select(select_statement3)[0]["heartbeat"]);

		}

        [Test()]
        public void TestDeadRemoval() {
			HearbeatsTable htable1 = new HearbeatsTable();
			htable1.AddEntry(1, 25);
			htable1.AddEntry(2, 35);
			htable1.AddEntry(3, 34);
			String select_statement1 = String.Format("process_id = 1");
            DateTime update_time = (DateTime)htable1.HTable.Select(select_statement1)[0]["last_updated"];
            htable1.HTable.Select(select_statement1)[0]["last_updated"] = update_time - new TimeSpan(0,0,20);
            htable1.RemoveDead();
            Assert.AreEqual(2, htable1.HTable.Rows.Count);
        }
    }

}
